#include "CameraPose.h"

CameraPose::CameraPose() {
	indexesDefined.push_back(false);
	indexesDefined.push_back(false);
	indexesDefined.push_back(false);
	indexesDefined.push_back(false);
}

CameraPose::~CameraPose() {

}

int CameraPose::initialize(cv::Mat intrinsic, int minHessian) {
	this->isInitialized = true;
	this->intrinsic = intrinsic.clone();
	this->K = this->intrinsic;
	this->minHessian = minHessian;
	this->rotEuler = std::vector<float>(3);
//#if HAVE_NONFREE
//	detector = cv::xfeatures2d::SURF::create(this->minHessian);
//#else
//	detector = cv::AKAZE::create();
//#endif
//
//#if USE_FLANN_MATCHER
//	matcher = new cv::FlannBasedMatcher();
//#else
//	matcher = cv::BFMatcher::create(cv::NORM_HAMMING);
//#endif

	matcher = cv::cuda::DescriptorMatcher::createBFMatcher(cv::NORM_HAMMING); //for orb
	orb = cv::cuda::ORB::create(2000, 1.5f, 5, 10, 0, 2, 0, 10);
	orb->setBlurForDescriptor(true);
	
	// Others
	indexDefined = false;
	return 0;
}

int CameraPose::initialize(cv::Mat intrinsic) {
	minHessian = 2000;
	this->initialize(intrinsic, minHessian);
	return 0;
}

int CameraPose::initialize(double focal, cv::Point2d pp) {
	minHessian = 2000;
	this->initialize(focal, pp, minHessian);
	return 0;
}

int CameraPose::initialize(double focal, cv::Point2d pp, int minHessian) {
	double k[9] = { focal, 0.0, (double)pp.x, 0.0, focal, (double)pp.y, 0.0, 0.0, 1.0 };
	intrinsic = cv::Mat(3, 3, CV_64F, k);
	this->initialize(intrinsic, minHessian);
	return 0;
}

int CameraPose::initialize(double focalx, double focaly, cv::Point2d pp, int minHessian) {
	double k[9] = { focalx, 0.0, (double)pp.x, 0.0, focaly, (double)pp.y, 0.0, 0.0, 1.0 };
	intrinsic = cv::Mat(3, 3, CV_64F, k);
	this->initialize(intrinsic, minHessian);
	return 0;
}

int CameraPose::initialize(double focalx, double focaly, double ppx, double ppy, int minHessian) {
	double k[9] = { focalx, 0.0, ppx, 0.0, focaly, ppy, 0.0, 0.0, 1.0 };
	intrinsic = cv::Mat(3, 3, CV_64F, k);
	this->initialize(intrinsic, minHessian);
	return 0;
}

int CameraPose::loadPanoramicImages(std::string im1filename, std::string im2filename, float angle) {
	cv::Mat im1pano = cv::imread(im1filename);
	cv::Mat im2pano = cv::imread(im2filename);
	if (im1pano.empty()) {
		throw Exception(im1filename + " not found x.");
	}
	if (im2pano.empty()) {
		throw Exception(im2filename + " not found.");
	}

	// Convert panoramic to cartesian
	cv::Mat im1loaded, im2loaded;
	panoramicToCartesian(im1pano, im1loaded, 320.0f, 1200.0f, angle, 3.1416f / 4.0f);
	panoramicToCartesian(im2pano, im2loaded, 320.0f, 1200.0f, angle, 3.1416f / 4.0f);

	if (im1loaded.channels() == 3) {
		cv::Mat im1gray;
		cv::Mat im2gray;
		cv::cvtColor(im1loaded, im1gray, CV_BGR2GRAY);
		cv::cvtColor(im2loaded, im2gray, CV_BGR2GRAY);
		this->im1 = im1gray;
		this->im2 = im2gray;
	}
	else {
		this->im1 = im1loaded;
		this->im2 = im2loaded;
	}

	/*std::cout << "K: << " << angle << std::endl;
	cv::imshow("im1", im1);
	cv::imshow("im2", im2);
	cv::waitKey();*/

	return 0;
}

int CameraPose::loadPanoramicImages(std::string im1filename, std::string im2filename) {
	loadPanoramicImages(im1filename, im2filename, 3.1416f);
	//cv::Mat im1pano = cv::imread(im1filename);
	//cv::Mat im2pano = cv::imread(im2filename);
	//if (im1pano.empty()) {
	//	throw Exception(im1filename + " not found x.");
	//}
	//if (im2pano.empty()) {
	//	throw Exception(im2filename + " not found.");
	//}

	//// Convert panoramic to cartesian
	//cv::Mat im1loaded, im2loaded;
	//panoramicToCartesian(im1pano, im1loaded, 320.0f, 768.0f, 3.1416f, 3.1416f / 4.0f);
	//panoramicToCartesian(im2pano, im2loaded, 320.0f, 768.0f, 3.1416f, 3.1416f / 4.0f);

	//if (im1loaded.channels() == 3) {
	//	cv::Mat im1gray;
	//	cv::Mat im2gray;
	//	cv::cvtColor(im1loaded, im1gray, CV_BGR2GRAY);
	//	cv::cvtColor(im2loaded, im2gray, CV_BGR2GRAY);
	//	this->im1 = im1gray;
	//	this->im2 = im2gray;
	//}
	//else {
	//	this->im1 = im1loaded;
	//	this->im2 = im2loaded;
	//}
	return 0;
}

int CameraPose::loadImages(std::string im1filename, std::string im2filename) {
	cv::Mat im1loaded = cv::imread(im1filename);
	cv::Mat im2loaded = cv::imread(im2filename);
	if (im1loaded.empty()) {
		throw Exception(im1filename + " not found x.");
	}
	if (im2loaded.empty()) {
		throw Exception(im2filename + " not found.");
	}

	if (im1loaded.channels() == 3) {
		cv::Mat im1gray;
		cv::Mat im2gray;
		cv::cvtColor(im1loaded, im1gray, CV_BGR2GRAY);
		cv::cvtColor(im2loaded, im2gray, CV_BGR2GRAY);
		this->im1 = im1gray;
		this->im2 = im2gray;
	}
	else {
		this->im1 = im1loaded;
		this->im2 = im2loaded;
	}
	return 0;
}

int CameraPose::panoramicToCartesian(cv::Mat input, cv::Mat &output, float height, float width, float dirx, float angleofview) {
	//output = input.clone();
	int c = input.channels();
	float h = (float)input.rows;
	float w = (float)input.cols;

	float viewangle = angleofview;
	float focal;

	if (width > height) {
		focal = (height / 2.0f) / tan(viewangle / 2.0f);
	}
	else {
		focal = (width / 2.0f) / tan(viewangle / 2.0f);
	}

	//if (!indexDefined) {
		cv::Mat idxArr(1, (int)width, CV_32F);
		cv::Mat idyArr((int)height, 1, CV_32F);
		for (int j = 0; j < height; j++) {
			idyArr.at<float>(j, 0) = (float)j;
		}
		for (int i = 0; i < width; i++) {
			idxArr.at<float>(0, i) = (float)i;
		}

		this->idx = cv::repeat(idxArr, (int)height, 1);
		this->idy = cv::repeat(idyArr, 1, (int)width);
		/*std::cout << idx.size() << std::endl;
		std::cout << idy.size() << std::endl;

		cv::imshow("idx", idx / width);
		cv::imshow("idy", idy / height);
		cv::waitKey();*/

		// Get equivalent angles of the cartesian coordinates
		float corx, cory;
		theta = cv::Mat::zeros(cv::Size((int)width, (int)height), CV_32F);
		phi = cv::Mat::zeros(cv::Size((int)width, (int)height), CV_32F);

		for (int j = 0; j < height; j++) {
			for (int i = 0; i < width; i++) {
				corx = idx.at<float>(j, i) - width / 2.0f;
				cory = -(idy.at<float>(j, i) - height / 2.0f);
				theta.at<float>(j, i) = atan2(corx, focal);
				phi.at<float>(j, i) = atan2(cory * cos(theta.at<float>(j, i)), focal);
				theta.at<float>(j, i) = theta.at<float>(j, i) + dirx;

				//std::cout << theta.at<float>(j, i) << std::endl;
			}
		}
		indexDefined = true;
	//}
	
	if (c == 3) {
		output = cv::Mat::zeros(cv::Size((int)width, (int)height), CV_8UC3);
	}
	else {
		output = cv::Mat::zeros(cv::Size((int)width, (int)height), CV_8U);
	}

	for (int j = 0; j < height; j++) {
		for (int i = 0; i < width; i++) {
			float x = w * theta.at<float>(j, i) / (2.0f * 3.1416f);
			float y = h / 2.0f - (phi.at<float>(j, i) / (3.1416f / 2.0f))*(h / 2.0f);
			if (y > h) {
				y = h;
			}
			if (x > w) {
				x = x - w;
			}
			else if (x < 0) {
				x = w + x;
			}

			// Bilinear interpolation
			int fx = (int)std::floor(x);
			int cx = (int)std::ceil(x);
			int fy = (int)std::floor(y);
			int cy = (int)std::ceil(y);

			if (cx > w) {
				cx = cx - (int)w;
			}
			else if (cx < 0) {
				cx = (int)w + cx;
			}

			if (fx > w) {
				fx = fx - (int)w;
			}
			else if (fx < 0) {
				fx = (int)w + fx;
			}

			float xd = x - fx;
			float yd = y - fy;

			if (c == 3) {
				output.at<cv::Vec3b>(j, i)[0] = (unsigned char)(
					(float)input.at<cv::Vec3b>(fy, fx)[0] * (1 - xd) * (1 - yd) +
					(float)input.at<cv::Vec3b>(fy, cx)[0] * xd * (1 - yd) +
					(float)input.at<cv::Vec3b>(cy, fx)[0] * (1 - xd) * yd +
					(float)input.at<cv::Vec3b>(cy, cx)[0] * xd * yd);
				output.at<cv::Vec3b>(j, i)[1] = (unsigned char)(
					(float)input.at<cv::Vec3b>(fy, fx)[1] * (1 - xd) * (1 - yd) +
					(float)input.at<cv::Vec3b>(fy, cx)[1] * xd * (1 - yd) +
					(float)input.at<cv::Vec3b>(cy, fx)[1] * (1 - xd) * yd +
					(float)input.at<cv::Vec3b>(cy, cx)[1] * xd * yd);
				output.at<cv::Vec3b>(j, i)[2] = (unsigned char)(
					(float)input.at<cv::Vec3b>(fy, fx)[2] * (1 - xd) * (1 - yd) +
					(float)input.at<cv::Vec3b>(fy, cx)[2] * xd * (1 - yd) +
					(float)input.at<cv::Vec3b>(cy, fx)[2] * (1 - xd) * yd +
					(float)input.at<cv::Vec3b>(cy, cx)[2] * xd * yd);
			}
			else {
				output.at<unsigned char>(j, i) = (unsigned char)(
					(float)input.at<unsigned char>(fy, fx) * (1 - xd) * (1 - yd) +
					(float)input.at<unsigned char>(fy, cx) * xd * (1 - yd) +
					(float)input.at<unsigned char>(cy, fx) * (1 - xd) * yd +
					(float)input.at<unsigned char>(cy, cx) * xd * yd);
			}
		}
	}
	/*cv::imshow("output", output);
	cv::waitKey();*/

	return 0;
}


// New
int CameraPose::loadPanoramicImages(std::string im1filename, std::string im2filename, int mapIndex) {
	cv::Mat im1pano = cv::imread(im1filename);
	cv::Mat im2pano = cv::imread(im2filename);
	if (im1pano.empty()) {
		throw Exception(im1filename + " not found x.");
	}
	if (im2pano.empty()) {
		throw Exception(im2filename + " not found.");
	}

	// Convert panoramic to cartesian
	cv::Mat im1loaded, im2loaded;
	panoramicToCartesian(im1pano, im1loaded, mapIndex);
	panoramicToCartesian(im2pano, im2loaded, mapIndex);
	//std::cout << "Map Index: " << mapIndex << std::endl;

	/*cv::namedWindow("xx", cv::WINDOW_NORMAL);
	cv::resizeWindow("xx", 540, 270);
	cv::imshow("xx", im1pano);*/

	if (im1loaded.channels() == 3) {
		cv::Mat im1gray;
		cv::Mat im2gray;
		cv::cvtColor(im1loaded, im1gray, CV_BGR2GRAY);
		cv::cvtColor(im2loaded, im2gray, CV_BGR2GRAY);
		this->im1 = im1gray;
		this->im2 = im2gray;

		/*cv::imshow("im1", im1);
		cv::imshow("im2", im2);
		cv::waitKey();*/
	}
	else {
		this->im1 = im1loaded;
		this->im2 = im2loaded;
	}
	return 0;
}

int CameraPose::createMapPanoToCartesian(float srcHeight, float srcWidth, float dstHeight, float dstWidth, float dirx, float angleofview) {
	float viewangle = angleofview;
	float focal;

	if (dstWidth > dstHeight) {
		focal = (dstHeight / 2.0f) / tan(viewangle / 2.0f);
	}
	else {
		focal = (dstWidth / 2.0f) / tan(viewangle / 2.0f);
	}

	cv::Mat idxArr(1, (int)dstWidth, CV_32F);
	cv::Mat idyArr((int)dstHeight, 1, CV_32F);
	for (int j = 0; j < dstHeight; j++) {
		idyArr.at<float>(j, 0) = (float)j;
	}
	for (int i = 0; i < dstWidth; i++) {
		idxArr.at<float>(0, i) = (float)i;
	}

	this->idx = cv::repeat(idxArr, (int)dstHeight, 1);
	this->idy = cv::repeat(idyArr, 1, (int)dstWidth);
	/*std::cout << idx.size() << std::endl;
	std::cout << idy.size() << std::endl;*/
	
	/*cv::imshow("idx", idx / width);
	cv::imshow("idy", idy / height);
	cv::waitKey();*/

	// Get equivalent angles of the cartesian coordinates
	float corx, cory;
	theta = cv::Mat::zeros(cv::Size((int)dstWidth, (int)dstHeight), CV_32F);
	phi = cv::Mat::zeros(cv::Size((int)dstWidth, (int)dstHeight), CV_32F);
	cv::Mat xMap = cv::Mat::zeros(cv::Size((int)dstWidth, (int)dstHeight), CV_32F);
	cv::Mat yMap = cv::Mat::zeros(cv::Size((int)dstWidth, (int)dstHeight), CV_32F);

	for (int j = 0; j < dstHeight; j++) {
		for (int i = 0; i < dstWidth; i++) {
			corx = idx.at<float>(j, i) - dstWidth / 2.0f;
			cory = -(idy.at<float>(j, i) - dstHeight / 2.0f);
			theta.at<float>(j, i) = (float)atan2(corx, focal);
			phi.at<float>(j, i) = (float)atan2(cory * cos(theta.at<float>(j, i)), focal);
			theta.at<float>(j, i) = theta.at<float>(j, i) + dirx;

			float x = srcWidth * theta.at<float>(j, i) / (2.0f * 3.1416f);
			float y = (srcHeight / 2.0f) - (phi.at<float>(j, i) / (3.1416f / 2.0f))*(srcHeight / 2.0f);

			if (y > srcHeight) {
				y = srcHeight;
			}
			if (x > srcWidth) {
				x = x - srcWidth;
			}
			else if (x < 0) {
				x = srcWidth + x;
			}

			//std::cout << theta.at<float>(j, i) << ":" << x << std::endl;
			
			xMap.at<float>(j, i) = x;
			yMap.at<float>(j, i) = y;
		}
	}
	/*cv::Mat xMap = width * theta / (2.0f * 3.1416f);
	cv::Mat yMap = height / 2.0f - (phi / (3.1416f / 2.0f))*(height / 2.0f);*/

	/*cv::Mat xMask = (xMap > width);
	xMap.setTo(xMap - width, xMask);
	xMask = (xMap < 0);
	xMap.setTo(width + xMap, xMask);
	cv::Mat yMask = yMap > height;
	yMap.setTo(height, yMask);*/

	/*cv::Mat xmap8;
	xMap.convertTo(xmap8, CV_8UC1, 256.0 / 768.0);
	cv::imshow("xmap", xmap8);
	cv::waitKey();*/

	/*cv::imshow("theta", theta);
	cv::waitKey();*/

	xMaps.push_back(xMap.clone());
	yMaps.push_back(yMap.clone());
	std::cout << "Created Maps" << std::endl;
	//std::cout << xMap << std::endl;
	return 0;
}

int CameraPose::panoramicToCartesian(cv::Mat input, cv::Mat &output, int mapIndex) {
	cv::remap(input, output, xMaps[mapIndex], yMaps[mapIndex], cv::INTER_LINEAR, cv::BORDER_CONSTANT);
	return 0;
}




int CameraPose::solvePose_8uc1(cv::Mat im1, cv::Mat im2) {
	this->im1 = im1;
	this->im2 = im2;
	this->d_im1.upload(im1);
	this->d_im2.upload(im2);

	orb->detectAndCompute(this->d_im1, cv::cuda::GpuMat(), keypoints_im1, d_descriptors_im1);
	orb->detectAndCompute(this->d_im2, cv::cuda::GpuMat(), keypoints_im2, d_descriptors_im2);

	if ((keypoints_im1.empty() || keypoints_im2.empty()) || (d_descriptors_im1.cols <= 1) || (d_descriptors_im2.cols <= 1)) {
		throw Exception("No keypoints found.");
	}
	else {
		//std::cout << "Keypoints found. Trying to match...";
		//std::cout << descriptors_im1.type() << " " << CV_32F << std::endl;
		//descriptors_im1.convertTo(descriptors_im1, CV_32F, 1 / 256.0f);
		//descriptors_im2.convertTo(descriptors_im2, CV_32F, 1 / 256.0f);
		matcher->knnMatch(d_descriptors_im1, d_descriptors_im2, matches, 2);
		//std::cout << "DONE" << std::endl;
		
		if (!matches.empty()) {
			//std::cout << "Removing outliers..." << std::endl;
			matched_keypoints_im1 = std::vector< cv::KeyPoint >();
			matched_keypoints_im2 = std::vector< cv::KeyPoint >();
			matched_keypoints_im1.clear();
			matched_keypoints_im2.clear();

			matchedpoints_im1 = std::vector<cv::Point2f>();
			matchedpoints_im2 = std::vector<cv::Point2f>();
			matchedpoints_im1.clear();
			matchedpoints_im2.clear();

			matched_distance = std::vector< float >();
			matched_distance.clear();

			nMatches = (int)matches.size();

			for (int k = 0; k < (int)matches.size(); k++)
			{
				if ((matches[k][0].distance < 0.6*(matches[k][1].distance)) && ((int)matches[k].size() <= 2 && (int)matches[k].size() > 0))
				{
					matched_keypoints_im1.push_back(keypoints_im1[matches[k][0].queryIdx]);
					matched_keypoints_im2.push_back(keypoints_im2[matches[k][0].trainIdx]);
					matchedpoints_im1.push_back(keypoints_im1[matches[k][0].queryIdx].pt);
					matchedpoints_im2.push_back(keypoints_im2[matches[k][0].trainIdx].pt);
					matched_distance.push_back(matches[k][0].distance);
				}
			}

			if (!matchedpoints_im1.empty() && (matchedpoints_im1.size() >= 5)) {
				//E = cv::findEssentialMat(cv::Mat(matchedpoints_im1), cv::Mat(matchedpoints_im2), K, cv::RANSAC, 0.999, 0.5, keypoint_mask);
				E = cv::findEssentialMat(cv::Mat(matchedpoints_im1), cv::Mat(matchedpoints_im2), K, cv::LMEDS, 0.999, 0.5, keypoint_mask);
				filtered_keypoints_im1 = std::vector< cv::KeyPoint >();
				filtered_keypoints_im2 = std::vector< cv::KeyPoint >();
				filtered_matchedpoints_im1 = std::vector < cv::Point2f >();
				filtered_matchedpoints_im2 = std::vector < cv::Point2f >();
				for (int k = 0; k < keypoint_mask.rows; k++) {
					if (keypoint_mask.at<bool>(0, k) == 1) {
						filtered_keypoints_im1.push_back(matched_keypoints_im1[k]);
						filtered_keypoints_im2.push_back(matched_keypoints_im2[k]);
						filtered_matchedpoints_im1.push_back(matchedpoints_im1[k]);
						filtered_matchedpoints_im2.push_back(matchedpoints_im2[k]);
					}
				}
				if (E.rows != 3) {
					throw Exception("Invalid essential matrix.");
				}
				else {
					//cv::recoverPose(E, cv::Mat(matchedpoints_im1), cv::Mat(matchedpoints_im2), R, t, focal, pp);
					cv::recoverPose(E, cv::Mat(filtered_matchedpoints_im1), cv::Mat(filtered_matchedpoints_im2), K, R, t);
					cv::Vec3f euler = rotationMatrixToEulerAngles(R);
					rotEuler[0] = euler[0];
					rotEuler[1] = euler[1];
					rotEuler[2] = euler[2];
				}
				return 0;
			}
			else {
				throw Exception("Too few matches found.");
			}
		}
		else {
			throw Exception("No matches found.");
		}
	}
}

int CameraPose::solvePose_8uc3(cv::Mat im1, cv::Mat im2) {
	cv::Mat im1gray;
	cv::Mat im2gray;
	cv::cvtColor(im1, im1gray, CV_BGR2GRAY);
	cv::cvtColor(im2, im2gray, CV_BGR2GRAY);
	if (!this->solvePose_8uc1(im1gray, im2gray))
		return 0;
	else return 1;
}

int CameraPose::solvePose() {
	if (!this->solvePose_8uc1(this->im1, this->im2))
		return 0;
	else return 1;
}

int CameraPose::drawKeypoints() {
	cv::drawKeypoints(im1, filtered_keypoints_im1, im1_draw, cv::Scalar(0, 255, 0), cv::DrawMatchesFlags::DEFAULT);
	cv::drawKeypoints(im2, matched_keypoints_im2, im2_draw, cv::Scalar(0, 0, 255), cv::DrawMatchesFlags::DEFAULT);
	overlay_matrix(im1_draw, R, t);
	cv::imshow("output", im1_draw);
	cv::imshow("reference", im2_draw);
	//overlay_keyframe(im1_draw, im2_draw);
	cv::waitKey(1);
	return 0;
}

int CameraPose::saveDrawKeypoints(std::string filename1, std::string filename2) {
	cv::drawKeypoints(im1, filtered_keypoints_im1, im1_draw, cv::Scalar(0, 255, 0), cv::DrawMatchesFlags::DEFAULT);
	cv::drawKeypoints(im2, matched_keypoints_im2, im2_draw, cv::Scalar(0, 0, 255), cv::DrawMatchesFlags::DEFAULT);
	overlay_matrix(im1_draw, R, t);
	/*cv::imshow("output", im1_draw);
	cv::imshow("reference", im2_draw);*/
	cv::imwrite(filename1, im1_draw);
	cv::imwrite(filename2, im2_draw);
	//overlay_keyframe(im1_draw, im2_draw);
	cv::waitKey(1);
	return 0;
}

int CameraPose::rotateEulerOnYAxis(float angle) {
	cv::Vec3f rot(rotEuler[0], rotEuler[1], rotEuler[2]);
	cv::Mat rotMat = eulerAnglesToRotationMatrix(rot);

	cv::Vec3f rotY(0.0f, angle, 0.0f);
	cv::Mat rotYMat = eulerAnglesToRotationMatrix(rotY);
	//std::cout << rotYMat << std::endl;

	cv::Mat totalRotMat = rotYMat * rotMat;
	cv::Vec3f totalRot = rotationMatrixToEulerAngles(totalRotMat);
	rotEuler[0] = totalRot[0];
	rotEuler[1] = totalRot[1];
	rotEuler[2] = totalRot[2];

	return 0;
}
// UTILITIES
cv::Vec3f CameraPose::rotationMatrixToEulerAngles(cv::Mat &R)
{
	float sy = (float)sqrt(R.at<double>(0, 0) * R.at<double>(0, 0) + R.at<double>(1, 0) * R.at<double>(1, 0));

	bool singular = sy < 1e-6; // If

	float x, y, z;
	if (!singular)
	{
		//std::cout << "Rotation matrix is singular." << std::endl;
		x = (float)atan2(R.at<double>(2, 1), R.at<double>(2, 2));
		y = (float)atan2(-R.at<double>(2, 0), sy);
		z = (float)atan2(R.at<double>(1, 0), R.at<double>(0, 0));
	}
	else
	{
		x = (float)atan2(-R.at<double>(1, 2), R.at<double>(1, 1));
		y = (float)atan2(-R.at<double>(2, 0), sy);
		z = (float)0;
	}
	return cv::Vec3f(180 * x / 3.14159f, 180 * y / 3.14159f, 180 * z / 3.14159f);
}

cv::Mat CameraPose::eulerAnglesToRotationMatrix(cv::Vec3f &euler)
{
	// Calculate rotation about x axis
	cv::Mat R_x = (cv::Mat_<double>(3, 3) <<
		1, 0, 0,
		0, cos(euler[0]), -sin(euler[0]),
		0, sin(euler[0]), cos(euler[0])
		);

	// Calculate rotation about y axis
	cv::Mat R_y = (cv::Mat_<double>(3, 3) <<
		cos(euler[1]), 0, sin(euler[1]),
		0, 1, 0,
		-sin(euler[1]), 0, cos(euler[1])
		);

	// Calculate rotation about z axis
	cv::Mat R_z = (cv::Mat_<double>(3, 3) <<
		cos(euler[2]), -sin(euler[2]), 0,
		sin(euler[2]), cos(euler[2]), 0,
		0, 0, 1);


	// Combined rotation matrix
	cv::Mat R = R_z * R_y * R_x;

	return R;
}

std::string CameraPose::parse_decimal(double f) {
	std::stringstream string;
	if (f < 0) {
		//negative
		string.precision(5);
		string << std::fixed << f;
	}
	else {
		//positive or zero
		string.precision(5);
		string << std::fixed << "+" << f;
	}
	return string.str();
}

void CameraPose::overlay_matrix(cv::Mat &im, cv::Mat R1, cv::Mat t) {
	std::ostringstream message1, message2, message3;
	message1 << std::fixed << parse_decimal(R1.at<double>(0, 0)) << " " << parse_decimal(R1.at<double>(0, 1)) << " " << parse_decimal(R1.at<double>(0, 2)) << " " << parse_decimal(t.at<double>(0));
	message2 << std::fixed << parse_decimal(R1.at<double>(1, 0)) << " " << parse_decimal(R1.at<double>(1, 1)) << " " << parse_decimal(R1.at<double>(1, 2)) << " " << parse_decimal(t.at<double>(1));
	message3 << std::fixed << parse_decimal(R1.at<double>(2, 0)) << " " << parse_decimal(R1.at<double>(2, 1)) << " " << parse_decimal(R1.at<double>(2, 2)) << " " << parse_decimal(t.at<double>(2));
	cv::Mat overlay;
	double alpha = 0.3;
	im.copyTo(overlay);
	cv::rectangle(overlay, cv::Rect(0, 0, 400, 47), cv::Scalar(255, 255, 255), -1);
	cv::addWeighted(overlay, alpha, im, 1 - alpha, 0, im);
	//cv::rectangle(im, cv::Point(0, 0), cv::Point(256, 47), CV_RGB(255, 255, 255), CV_FILLED, cv::LINE_8, 0);
	cv::Scalar tc = CV_RGB(0, 0, 0);
	cv::putText(im, message1.str(), cv::Point(0, 10), cv::FONT_HERSHEY_PLAIN, 1, tc);
	cv::putText(im, message2.str(), cv::Point(0, 22), cv::FONT_HERSHEY_PLAIN, 1, tc);
	cv::putText(im, message3.str(), cv::Point(0, 34), cv::FONT_HERSHEY_PLAIN, 1, tc);
}
