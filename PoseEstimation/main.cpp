#include <stdio.h>
#include "CameraPose.h"

cv::Vec3f rotationMatrixToEulerAngles(cv::Mat &R)
{
	float sy = sqrt(R.at<double>(0, 0) * R.at<double>(0, 0) + R.at<double>(1, 0) * R.at<double>(1, 0));

	bool singular = sy < 1e-6; // If

	float x, y, z;
	if (!singular)
	{
		//std::cout << "Rotation matrix is singular." << std::endl;
		x = atan2(R.at<double>(2, 1), R.at<double>(2, 2));
		y = atan2(-R.at<double>(2, 0), sy);
		z = atan2(R.at<double>(1, 0), R.at<double>(0, 0));
	}
	else
	{
		x = atan2(-R.at<double>(1, 2), R.at<double>(1, 1));
		y = atan2(-R.at<double>(2, 0), sy);
		z = 0;
	}
	return cv::Vec3f(180 * x / 3.14159, 180 * y / 3.14159, 180 * z / 3.14159);
}

std::string parse_decimal(double f) {
	std::stringstream string;
	if (f < 0) {
		//negative
		string.precision(5);
		string << std::fixed << f;
	}
	else {
		//positive or zero
		string.precision(5);
		string << std::fixed << "+" << f;
	}
	return string.str();
}

void overlay_matrix(cv::Mat &im, cv::Mat R1, cv::Mat t) {
	std::ostringstream message1, message2, message3;
	message1 << std::fixed << parse_decimal(R1.at<double>(0, 0)) << " " << parse_decimal(R1.at<double>(0, 1)) << " " << parse_decimal(R1.at<double>(0, 2)) << " " << parse_decimal(t.at<double>(0));
	message2 << std::fixed << parse_decimal(R1.at<double>(1, 0)) << " " << parse_decimal(R1.at<double>(1, 1)) << " " << parse_decimal(R1.at<double>(1, 2)) << " " << parse_decimal(t.at<double>(1));
	message3 << std::fixed << parse_decimal(R1.at<double>(2, 0)) << " " << parse_decimal(R1.at<double>(2, 1)) << " " << parse_decimal(R1.at<double>(2, 2)) << " " << parse_decimal(t.at<double>(2));
	cv::Mat overlay;
	double alpha = 0.3;
	im.copyTo(overlay);
	cv::rectangle(overlay, cv::Rect(0, 0, 400, 47), cv::Scalar(255, 255, 255), -1);
	cv::addWeighted(overlay, alpha, im, 1 - alpha, 0, im);
	//cv::rectangle(im, cv::Point(0, 0), cv::Point(256, 47), CV_RGB(255, 255, 255), CV_FILLED, cv::LINE_8, 0);
	cv::Scalar tc = CV_RGB(0, 0, 0);
	cv::putText(im, message1.str(), cv::Point(0, 10), CV_FONT_HERSHEY_PLAIN, 1, tc);
	cv::putText(im, message2.str(), cv::Point(0, 22), CV_FONT_HERSHEY_PLAIN, 1, tc);
	cv::putText(im, message3.str(), cv::Point(0, 34), CV_FONT_HERSHEY_PLAIN, 1, tc);
}

int main() {
	cv::Mat im1_rgb = cv::imread("d:/dev/data/iwaki_pers/ladybug768_pi_3.png");
	cv::Mat im2_rgb = cv::imread("d:/dev/data/iwaki_pers/ladybug768_pi_2.png");
	cv::Mat im1, im2;
	cv::Mat im1_draw, im2_draw;
	cv::resize(im1_rgb, im1, cv::Size(768, 320));
	cv::resize(im2_rgb, im2, cv::Size(768, 320));
	cv::Mat R, t;

	CameraPose *camerapose = new CameraPose();
	camerapose->initialize(386.2742, 386.2742, cv::Point2d(384, 160), 2000);
	camerapose->solvePose_8uc3(im1, im2, R, t);
	cv::Vec3f rotMat = rotationMatrixToEulerAngles(R);
	std::cout << rotMat << std::endl;

	drawKeypoints(im1, camerapose->filtered_keypoints_im1, im1_draw, cv::Scalar(0, 255, 0), cv::DrawMatchesFlags::DEFAULT);
	drawKeypoints(im2, camerapose->matched_keypoints_im2, im2_draw, cv::Scalar(0, 0, 255), cv::DrawMatchesFlags::DEFAULT);
	overlay_matrix(im1_draw, R, t);
	cv::imshow("output", im1_draw);
	cv::imshow("reference", im2_draw);
	//overlay_keyframe(im1_draw, im2_draw);
	cv::waitKey();

	return 0;
}