#pragma once

#include "lib_link.h"
#include <opencv2/opencv.hpp>
#include <iostream>
#include <fstream>
#include <opencv2/tracking.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <opencv2/xfeatures2d/cuda.hpp>
#include <opencv2/cudafeatures2d.hpp>

//#define USE_ORB true
//#define USE_FLANN_MATCHER true

class CameraPose {
public:
	CameraPose();
	~CameraPose();

	bool isInitialized = false;

	int loadImages(std::string im1filename, std::string im2filename);
	int loadPanoramicImages(std::string im1filename, std::string im2filename);
	int loadPanoramicImages(std::string im1filename, std::string im2filename, float angle);
	int panoramicToCartesian(cv::Mat input, cv::Mat &output, float height, float width, float dirx, float angleofview);
	
	// New
	int createMapPanoToCartesian(float srcHeight, float srcWidth, float height, float width, float dirx, float angleofview);
	int panoramicToCartesian(cv::Mat input, cv::Mat &output, int mapIndex);
	int loadPanoramicImages(std::string im1filename, std::string im2filename, int mapIndex);

	std::vector<cv::Mat> xMaps;
	std::vector<cv::Mat> yMaps;

	int initialize(cv::Mat intrinsic);
	int initialize(cv::Mat intrinsic, int minHessian);
	int initialize(double focal, cv::Point2d pp);
	int initialize(double focal, cv::Point2d pp, int minHessian);
	int initialize(double focalx, double focaly, cv::Point2d pp, int minHessian);
	int initialize(double focalx, double focaly, double ppx, double ppy, int minHessian);

	int solvePose_8uc3(cv::Mat im1, cv::Mat im2);
	int solvePose_8uc1(cv::Mat im1, cv::Mat im2);
	int solvePose();

	int drawKeypoints();
	int saveDrawKeypoints(std::string filename1, std::string filename2);

	int rotateEulerOnYAxis(float angle);

	cv::Mat rotation; // 3x3 matrix
	cv::Mat translation;
	cv::Mat R;
	cv::Mat t;
	std::vector<float> rotEuler;
	int nMatches;

	cv::Mat intrinsic; //3x3 matrix
	cv::Mat K;

	double focal;
	cv::Point2d pp;

	// Panoramic to cartesian conversion
	cv::Mat idx, idy;
	cv::Mat theta, phi;
	bool indexDefined;
	std::vector<bool> indexesDefined;

	//UTILITIES
	void overlay_matrix(cv::Mat &im, cv::Mat R1, cv::Mat t);
	std::string parse_decimal(double f);
	cv::Vec3f rotationMatrixToEulerAngles(cv::Mat &R);
	cv::Mat eulerAnglesToRotationMatrix(cv::Vec3f &euler);

	cv::Mat im1, im2;
	cv::Mat im1_rgb, im2_rgb;
	cv::Mat im1_draw, im2_draw;
	std::vector< cv::KeyPoint > keypoints_im1, keypoints_im2;
	//cv::Mat descriptors_im1, descriptors_im2;
	cv::cuda::GpuMat d_descriptors_im1, d_descriptors_im2;
	cv::Mat pose;

	int minHessian = 1000;

//#if HAVE_NONFREE
//	cv::Ptr<cv::xfeatures2d::SURF> detector;
//#else
//	cv::Ptr<cv::AKAZE> detector;
//#endif
//
//#if USE_FLANN_MATCHER
//	cv::FlannBasedMatcher * matcher;
//#else
//	cv::BFMatcher * matcher;
//#endif

	cv::Ptr<cv::cuda::ORB> orb;
	cv::Ptr< cv::cuda::DescriptorMatcher > matcher;

	std::vector< std::vector< cv::DMatch> > matches;

	std::vector<cv::Point2f> matchedpoints_im1;
	std::vector<cv::Point2f> matchedpoints_im2;
	std::vector<cv::Point2f> filtered_matchedpoints_im1;
	std::vector<cv::Point2f> filtered_matchedpoints_im2;
	std::vector< cv::KeyPoint > matched_keypoints_im1;
	std::vector< cv::KeyPoint > matched_keypoints_im2;
	std::vector< float > matched_distance;
	std::vector< cv::KeyPoint > filtered_keypoints_im1;
	std::vector< cv::KeyPoint > filtered_keypoints_im2;

	cv::Mat E;
	cv::Mat keypoint_mask;

	cv::cuda::GpuMat d_im1;
	cv::cuda::GpuMat d_im2;
};

//// Error-handling
class Exception : public std::exception {
public:
	Exception(std::string msg) {
		this->msg = msg;
	};
	std::string msg;
	virtual char const* what() const throw() {
		return msg.c_str();
	}
};