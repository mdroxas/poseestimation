#include <fstream>
#include "../PoseEstimation/CameraPose.h"
#include <stdio.h>
#include <filesystem>

int solvePose(CameraPose *cameraPose, int k, std::string im1, std::string im2, float &rx, float &ry, float &rz) {
	float angle = 3.14159f / 2.0f;
	//CameraPose *camerapose = new CameraPose();
	// Initialize solver
	cv::Mat input = cv::imread(im1);
	if (!cameraPose->isInitialized) {
		cameraPose->initialize(386.2742, 386.2742, 384.0, 160.0, 2000);
		for (int view = 0; view < 4; view++) {
			cameraPose->createMapPanoToCartesian((float)input.rows, (float)input.cols, 320.0f, 768.0f, angle*(float)view, 3.1416f / 4.0f);
		}
	}

	try {
		//camerapose->loadPanoramicImages(im1, im2);
		//cameraPose->loadPanoramicImages(im1, im2, angle*(float)k);
		cameraPose->loadPanoramicImages(im1, im2, k);
		//im2 = im1;
	}
	catch (Exception ex) {
		std::cerr << ex.what() << std::endl;
		return -1;
	}

	// Calculate pose
	try {
		cameraPose->solvePose();
		/*cameraPose->drawKeypoints();*/
	}
	catch (Exception ex) {
		std::cerr << ex.what() << std::endl;
		return -1;
	}

	//camerapose->drawKeypoints();
	//std::cout << camerapose->rotEuler[0] << " " << camerapose->rotEuler[1] << " " << camerapose->rotEuler[2] << std::endl;
	// Rotate based on the angle of view
	//camerapose->rotateEulerOnYAxis(2.0f*angle - angle * (float)k);
	// Switch axis
	float swapX, swapY, swapZ;
	if (k == 0) {
		swapX = -cameraPose->rotEuler[0];
		swapY = cameraPose->rotEuler[1];
		swapZ = -cameraPose->rotEuler[2];
		//x = -x
		//z = -z
	}
	else if (k == 1) {
		swapX = -cameraPose->rotEuler[2];
		swapY = cameraPose->rotEuler[1];
		swapZ = cameraPose->rotEuler[0];
		//x = -z
		//z = x
	}
	else if (k == 2) {
		swapX = cameraPose->rotEuler[0];
		swapY = cameraPose->rotEuler[1];
		swapZ = cameraPose->rotEuler[2];
		//nothing
	}
	else if (k == 3) {
		swapX = cameraPose->rotEuler[2];
		swapY = cameraPose->rotEuler[1];
		swapZ = -cameraPose->rotEuler[0];
		//x = z
		//z = -x
	}
	/*if (k == 2) {
		camerapose->drawKeypoints();
	}*/
	// Collect the pose
	/*rot0.push_back(swapX);
	rot1.push_back(swapY);
	rot2.push_back(swapZ);*/

	//std::cout << k << ":: " << swapX << " " << swapY << " " << swapZ << " matches: " << camerapose->nMatches << std::endl;
	//std::cout << camerapose->rotEuler[0] << " " << camerapose->rotEuler[1] << " " << camerapose->rotEuler[2] << std::endl;
	//std::cout << camerapose->nMatches << std::endl << std::endl;
	//delete camerapose;
	rx = swapX;
	ry = swapY;
	rz = swapZ;
	return 0;
}

int main(int argc, char *argv[]) {
	std::string im1, im2 = "";
	std::string list;
	std::string outputFilename;
	CameraPose *camerapose = new CameraPose();

	// Read command line arguments
	//std::cout << argc << std::endl;
	/*if (argc == 1) {
		im1 = "h:/data_iwaki/Iwaki2_R6-S_panoS/image768_pi/ladybug768_pi_3.png";
		im2 = "h:/data_iwaki/Iwaki2_R6-S_panoS/image768_pi/ladybug768_pi_2.png";
	}*/

	// Solve one image pair
	//if (argc == 4) {
	//	im1 = std::string(argv[1]);
	//	im2 = std::string(argv[2]);
	//	outputFilename = std::string(argv[3]);

	//	if (outputFilename.substr(outputFilename.find_last_of(".") + 1) != "txt") {
	//		std::cerr << outputFilename << " not a txt file." << std::endl;
	//		return 1;
	//	}

	//	CameraPose *camerapose = new CameraPose();
	//	// Initialize solver
	//	camerapose->initialize(386.2742, 386.2742, 384.0, 160.0, 2000);

	//	// Open image pairs
	//	try {
	//		camerapose->loadPanoramicImages(im1, im2);
	//	}
	//	catch (Exception ex) {
	//		std::cerr << ex.what() << std::endl;
	//		return 1;
	//	}

	//	// Calculate pose
	//	try {
	//		camerapose->solvePose();
	//	}
	//	catch (Exception ex) {
	//		std::cerr << ex.what() << std::endl;
	//		return 1;
	//	}

	//	std::ofstream outFile;
	//	outFile.exceptions(std::ofstream::failbit);
	//	try {
	//		outFile.open(outputFilename);
	//	}
	//	catch (const std::exception& ex) {
	//		std::cerr << "Error writing on: " << outputFilename << std::endl;
	//		return 1;
	//	}
	//	outFile << camerapose->rotEuler[0] << " " << camerapose->rotEuler[1] << " " << camerapose->rotEuler[2] << std::endl;
	//	//std::cout << camerapose->rotEuler[0] << " " << camerapose->rotEuler[1] << " " << camerapose->rotEuler[2] << std::endl;
	//	//camerapose->drawKeypoints();
	//	outFile.close();
	//}
	
	if ((argc == 3)||(argc == 5)||(argc == 2)||(argc == 4)) {
		int startFrame = 0;
		int endFrame;
		if (argc == 5) {
			startFrame = atoi(argv[3]);
			endFrame = atoi(argv[4]);
		}
		else if (argc == 4) {
			startFrame = atoi(argv[2]);
			endFrame = atoi(argv[3]);
		}

		std::ifstream file;
		// Solve a list
		if ((argc == 3) || (argc == 5)) {
			list = argv[1];
			
			file.exceptions(std::ifstream::failbit);
			try {
				file.open(list);
			}
			catch (Exception ex) {
				std::cerr << list << " text file not found. " << std::endl;
				return 1;
			}
		}
		else if ((argc == 2)|| (argc == 4)) {
			// Open directory
			std::ofstream exFile("tempImageList.txt");
			int skipCnt = 0;
			for (const auto & entry : std::filesystem::directory_iterator(argv[1])) {
				//std::cout << entry.path().string() << std::endl;
				if ((skipCnt >= startFrame) && (skipCnt <= endFrame)) {
					exFile << entry.path().string() << std::endl;
				}
				skipCnt++;
			}
			exFile.close();
			file.open("tempImageList.txt");
			//file.seekg(0);
			//file.seekg(0, std::ios::beg);
		}
		
		if ((argc != 2) && (argc != 4)) {
			outputFilename = std::string(argv[2]);
		}
		else {
			outputFilename = std::string("output.txt");
		}
		
		if (outputFilename.substr(outputFilename.find_last_of(".") + 1) != "txt") {
			std::cerr << outputFilename << " not a txt file." << std::endl;
			return 1;
		}
		
		std::ofstream outFile;
		outFile.exceptions(std::ofstream::failbit);
		try {
			outFile.open(outputFilename);
		}
		catch (Exception ex) {
			std::cerr << "Error writing on: " << outputFilename << std::endl;
			return 1;
		}

		int cnt = startFrame;
		float prevrx = 0.0f;
		float prevry = 0.0f;
		float prevrz = 0.0f;

		
		while (std::getline(file, im1, '\n')) {
			std::cout << "Processing pairs: " << cnt + 1 << std::endl;
			if (cnt == startFrame) {
				im2 = im1;
			}
			//else if (((argc == 5)||(argc == 4)) && (cnt < startFrame)) {
			//	//skip
			//	im2 = im1;
			//	std::cout << "skip: " << im2 << std::endl;
			//}
			else {
				// Open image pairs
				float angle = 3.14159f/2.0f;
				std::vector<float> rotx;
				std::vector<float> roty;
				std::vector<float> rotz;

				std::vector<float> finalRot(3);
				float outputrx = prevrx;
				float outputry = prevry;
				float outputrz = prevrz;
				float smallestDiff = 1000.0f;
				for (int k = 0; k < 4; k++) {
					float rx, ry, rz;
					if (solvePose(camerapose, k, im1, im2, rx, ry, rz) == 0) {
						//push the value
						/*rotx.push_back(rx);
						roty.push_back(ry);
						rotz.push_back(rz);*/
						//std::cout << k << ":: " << rx << " " << ry << " " << rz << std::endl;
						//float currDiff = sqrt((rx - prevrx)*(rx - prevrx) + (ry - prevry)*(ry - prevry) + (rz - prevrz)*(rz - prevrz));
						float currDiff = sqrt((ry - prevry)*(ry - prevry));
						//std::cout << currDiff << " ";
						if ((currDiff < smallestDiff) && (currDiff < 5.0f)) {
							smallestDiff = currDiff;
							outputrx = rx;
							outputry = ry;
							outputrz = rz;
						}
						/*std::string image1Name = "image1" + std::to_string(cnt) + "_" + std::to_string(k) + ".png";
						std::string image2Name = "image2" + std::to_string(cnt) + "_" + std::to_string(k) + ".png";
						camerapose->saveDrawKeypoints(image1Name, image2Name);*/
					}
					
				}
				//camerapose->drawKeypoints();

				//std::cout << std::endl;
				if ((abs(outputrx) > 10.0f) || (abs(outputry) > 10.0f) || (abs(outputrz) > 10.0f)) {
					outputrx = 0.0f;
					outputry = 0.0f;
					outputrz = 0.0f;
				}

				prevrx = outputrx;
				prevry = outputry;
				prevrz = outputrz;
				//std::cout << smallestDiff << ": " << outputrx << " " << outputry << " " << outputrz << std::endl;
				outFile << outputrx << "," << outputry << "," << outputrz << std::endl;
				//outFile << camerapose->rotEuler[0] << " " << camerapose->rotEuler[1] << " " << camerapose->rotEuler[2] << std::endl;
				//std::cout << camerapose->rotEuler[0] << " " << camerapose->rotEuler[1] << " " << camerapose->rotEuler[2] << std::endl;
				im2 = im1;
			}
			cnt++;
		}
		outFile.close();
		file.close();
	}

	else {
		std::cout << "  Usage: " << std::endl;
		//std::cout << "     *.exe [filename1] [filename2] [output text file]" << std::endl;
		/*std::cout << "     *.exe [text file with list of filenames filename1 " << std::endl;
		std::cout << "                                             filename2 " << std::endl;
		std::cout << "                                             ...       " << std::endl;
		std::cout << "                                             filenameN] [output text file]" << std::endl;*/
		std::cout << "     *.exe [path/to/panoramicimages] [optional: start frame] [optional: end frame] " << std::endl;
		std::cout << "     *.exe [text file with list of filenames] [output text file] [optional: start frame] [optional: end frame] " << std::endl;
		std::cout << "  Example:" << std::endl;
		//std::cout << "     poser.exe im1.png im2.png output.txt" << std::endl;

		std::cout << "     poser.exe E:/folder/images/" << std::endl;
		std::cout << "     poser.exe E:/folder/images/ 0 2200" << std::endl;
		std::cout << "     poser.exe imageList.txt output.txt" << std::endl;
		std::cout << "     poser.exe imageList.txt output.txt 0 2700" << std::endl;
		return 0;
	}

	return 0;
}